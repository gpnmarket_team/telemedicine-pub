package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
)

type msgBroker interface {
	Connect() error
	SendMsg(msg []byte) ([]byte, error)
}

type httpListener interface {
	StartListen() error
}

type bridge struct {
	broker   msgBroker
	listener httpListener
}

// Start will try to start message broker and http listener.
func (br *bridge) Start() error {
	if err := br.broker.Connect(); err != nil {
		return err
	}

	br.prepareRoutes()

	if err := br.listener.StartListen(); err != nil {
		return err
	}

	return nil
}

// NewBridge creates a new bridge between message broker and http listener.
func NewBridge(msgBroker msgBroker, listener httpListener) *bridge {
	return &bridge{
		broker:   msgBroker,
		listener: listener,
	}
}

func (br *bridge) prepareRoutes() {
	http.HandleFunc("/", br.rootHandler)
}

func (br *bridge) rootHandler(w http.ResponseWriter, r *http.Request) {


	defer func() {
		if err := r.Body.Close(); err != nil {
			log.Println(err)
		}
	}()

	if !validation(r) {
		w.WriteHeader(http.StatusNotFound)

		log.Printf("Fail valid for %s", r.URL.Path)
		return
	}
	dumped, err := httputil.DumpRequest(r, true)
	if err != nil {
		log.Println(err)
		return
	}
	log.Print(string(dumped))
	strResp, err := json.Marshal(map[string]string{
		"request":    string(dumped),
		"remoteAddr": r.RemoteAddr,
	})
	if err != nil {
		log.Println(err)
		return
	}

	respNats, err := br.broker.SendMsg(strResp)
	if err != nil {
		http.Error(w, "", http.StatusServiceUnavailable)
		return
	}

	b := bufio.NewReader(bytes.NewReader(respNats))
	resp, err := http.ReadResponse(b, r)
	if err != nil {
		log.Println(err)
		return
	}

	if err := pipeReq(w, resp); err != nil {
		log.Println(err)
		return
	}
}

func validation(r *http.Request) bool {
	//var data map[string]interface{}

	b, err := ioutil.ReadAll(r.Body)
	r.Body = ioutil.NopCloser(bytes.NewBuffer(b))

	if err != nil {
		log.Printf("Fail get request body")
		return false
	}

	return true 
}

func pipeReq(rw http.ResponseWriter, resp *http.Response) error {
	rw.Header().Set("Content-Type", resp.Header.Get("Content-Type"))
	rw.Header().Set("Content-Length", resp.Header.Get("Content-Length"))
	if (resp.Header.Get("Location") != "") {
		rw.Header().Set("Location", resp.Header.Get("Location"))
	}
	rw.WriteHeader(resp.StatusCode)
	if _, err := io.Copy(rw, resp.Body); err != nil {
		log.Printf("empty body")
		return nil
	}
	if err := resp.Body.Close(); err != nil {
		log.Printf("Fail 2")
		return err
	}
	return nil
}
