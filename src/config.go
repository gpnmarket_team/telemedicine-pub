package main

import (
	"flag"
	"log"
)

const (
	defaultListenerUri      = "0.0.0.0:9000"
	defaultMessageBrokerUri = "tls://127.0.0.1:4222"
)

type config struct {
	rootCertFiles    []string
	clientCertFile   string
	clientKeyFile    string
	messageBrokerUri string
	serverCertFile   string
	serverKeyFile    string
	listenerUri      string
	uName            string
	uPass            string
}

func parseCmdArgs() config {
	ca := flag.String("rootCa", "", "ca")

	cert := flag.String("clientCa", "", "client cert")
	key := flag.String("clientKey", "", "client key")

	scert := flag.String("serverCa", "", "server cert")
	skey := flag.String("serverKey", "", "server key")
	uName := flag.String("uName", "", "user name")
	uPass := flag.String("uPass", "", "user pass")
	tsHost := flag.String("tsHost", defaultMessageBrokerUri, "server url")
	p := flag.String("p", defaultListenerUri, "pub client port")

	flag.Parse()
	rootCertFiles := []string{*ca}
	if *ca == "" || *cert == "" || *key == "" {
		log.Fatal("need certificate path", *ca, *cert, *key)
	}

	return config{
		rootCertFiles:    rootCertFiles,
		clientCertFile:   *cert,
		clientKeyFile:    *key,
		messageBrokerUri: *tsHost,
		serverCertFile:   *scert,
		serverKeyFile:    *skey,
		listenerUri:      *p,
		uName:            *uName,
		uPass:            *uPass,
	}
}
