module gazpromneft/ittiopis/nats-client-pub

go 1.13

require (
	github.com/avast/retry-go v2.4.3+incompatible
	github.com/nats-io/nats.go v1.9.1
)
