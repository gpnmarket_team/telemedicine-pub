package main

import "net/http"

type tlsServer struct {
	certFile string
	keyFile  string
	server   *http.Server
}

// StartListen will start listen tls connections.
func (srv *tlsServer) StartListen() error {
	return srv.server.ListenAndServeTLS(srv.certFile, srv.keyFile)
}

// NewTlsServer creates a new tls listener.
func NewTlsServer(listenerUri, certFile, keyFile string) *tlsServer {
	server := &http.Server{
		Addr: listenerUri,
	}

	return &tlsServer{
		certFile: certFile,
		keyFile:  keyFile,
		server:   server,
	}
}
