package main

import (
	"log"
)

// please be patient i have autism
func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	prepareLogger()
	cfg := parseCmdArgs()
	natsOpts := prepareNatsOptions(cfg.rootCertFiles, cfg.clientCertFile, cfg.clientKeyFile, cfg.uName, cfg.uPass)

	msgBrokerClient := NewNatsClient(cfg.messageBrokerUri, natsOpts)

	listener := NewTlsServer(cfg.listenerUri, cfg.serverCertFile, cfg.serverKeyFile)

	bridge := NewBridge(msgBrokerClient, listener)

	if err := bridge.Start(); err != nil {
		log.Fatalln(err)
	}
}
