package main

import "log"

func prepareLogger() {
	log.SetFlags(log.LstdFlags|log.Lshortfile)
}
