package main

import (
	"log"
	"time"

	"github.com/avast/retry-go"
	"github.com/nats-io/nats.go"
)

const (
	clientName = "Public promotion client"
	subj       = "promocode"
)

type natsClient struct {
	uri     string
	options []nats.Option
	conn    *nats.Conn
}

// Connect will try to connect to NATS.
func (nc *natsClient) Connect() error {
	var attempts uint = 3
	var delay = 1 * time.Second

	return retry.Do(
		func() error {
			var err error

			nc.conn, err = nats.Connect(nc.uri, nc.options...)
			if err != nil {
				return err
			}

			log.Println("TS connect OK")
			log.Printf("nc max pay load [%v]\n", nc.conn.MaxPayload())

			return nil
		},
		retry.Attempts(attempts),
		retry.Delay(delay),
		retry.OnRetry(func(n uint, err error) {
			log.Printf("Connection to NATS - attempt: %d out of %d, error: %s\n", n, attempts, err)
		}),
	)
}

// SendMsg will send message to NATS.
func (nc *natsClient) SendMsg(payload []byte) ([]byte, error) {
	resp, err := nc.conn.Request(subj, payload, 30*time.Second)
	if err != nil {
		if nc.conn.LastError() != nil {
			log.Printf("%v for request", nc.conn.LastError())
		}
		log.Printf("%v for request", err)
		return nil, err
	}

	return resp.Data, err
}

// NewNatsClient creates a new NATS client.
func NewNatsClient(uri string, options []nats.Option) *natsClient {
	return &natsClient{
		uri:     uri,
		options: options,
	}
}

func prepareNatsOptions(rootCertFiles []string, clientCertFile, clientKeyFile string, uName string, uPass string) []nats.Option {
	caOpt := nats.RootCAs(rootCertFiles...)
	clentCertOpt := nats.ClientCert(clientCertFile, clientKeyFile)
	nameOpt := nats.Name(clientName)
	opts := []nats.Option{nameOpt, caOpt, clentCertOpt, nats.UserInfo(uName, uPass)}
	return setupConnOptions(opts)
}

func setupConnOptions(opts []nats.Option) []nats.Option {
	totalWait := 10 * time.Minute
	reconnectDelay := time.Second

	disconnectHandler := func(nc *nats.Conn, err error) {
		log.Printf("Disconnected: will attempt reconnects for %.0fm", totalWait.Minutes())
	}

	reconnectHandler := func(nc *nats.Conn) {
		log.Printf("Reconnected [%s]", nc.ConnectedUrl())
	}

	closedHandler := func(nc *nats.Conn) {
		log.Fatalln("Exiting, no servers available")
	}

	return append(opts,
		nats.ReconnectWait(reconnectDelay),
		nats.MaxReconnects(int(totalWait/reconnectDelay)),
		nats.DisconnectErrHandler(disconnectHandler),
		nats.ReconnectHandler(reconnectHandler),
		nats.ClosedHandler(closedHandler),
	)
}
