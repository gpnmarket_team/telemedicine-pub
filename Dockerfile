FROM artifactory.gazprom-neft.local/ci-docker-local/dev-base/rhel7-gpn

COPY ./addUserAndGroup.sh ./

RUN chmod +x ./addUserAndGroup.sh && ./addUserAndGroup.sh && rm -f ./addUserAndGroup.sh

ENV APP /opt/app

RUN mkdir -p $APP

WORKDIR $APP

COPY ./bin $APP/

RUN chown -R 1001000000:1500 $APP && chmod +x pub_client

USER 1001000000

ENTRYPOINT ./pub_client \
    -rootCa /run/secrets/ca.pem \
    -clientCa /run/secrets/client.pem \
    -clientKey /run/secrets/client.key \
    -serverCa /run/secrets/server.pem \
    -serverKey /run/secrets/server.key \
    -uName $PUB_USER -uPass $PUB_PASS \
    -p ":"$PUB_SERVER_PORT \
    -tsHost $NATS_HOST
