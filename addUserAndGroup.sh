#!/bin/bash

GROUP_ID=1500
groupadd -g ${GROUP_ID} app-users

for USER_ID in 1001000000 1000189999 1000369999; do
        useradd -u ${USER_ID} -g ${GROUP_ID} -G ${GROUP_ID} app-user${USER_ID} --no-log-init
done
